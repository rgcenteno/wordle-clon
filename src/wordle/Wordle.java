/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wordle;

import wordle.gui.WordleMain;
import wordle.wordleclass.IMotorIdioma;

/**
 *
 * @author rgcenteno
 */
public class Wordle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        lanzarWordle(new wordle.wordleclass.MotorTest());
    }
    
    /**
     * Se encarga de lanzar el juego con el motor de idioma pasado como parámetro
     * @param motor Motor de idioma que se utilizará en la partida
     * @throws NullPointerException si el motor pasado es null
     */
    public static void lanzarWordle(IMotorIdioma motor){
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WordleMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WordleMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WordleMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WordleMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>                
        /* Create and display the form */
        java.util.Objects.requireNonNull(motor, "El motor no puede ser null");
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WordleMain(motor).setVisible(true);
            }
        });
    }
    
}
