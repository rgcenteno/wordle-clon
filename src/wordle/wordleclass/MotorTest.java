/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wordle.wordleclass;

/**
 * Motor de test para pruebas. Siempre devuelve la palabra ABCDE y todas las palabras las marca como correctas
 * @author rgcenteno
 */
public class MotorTest implements IMotorIdioma{

    @Override
    public boolean checkPalabra(String palabra) {
        return true;
    }

    @Override
    public FixedLengthString obtenerPalabraAleatoria() {
        return new FixedLengthString("ABCDE");
    }
    
    
}
