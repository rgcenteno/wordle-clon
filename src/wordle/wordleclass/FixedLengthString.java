/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wordle.wordleclass;

/**
 * Clase realizada para asegurarnos de que sólo se insertan palabras de 5 letras en el juego wordle. Sólo se permiten palabras con letras A-Z o a-z sin tildes.
 * @author rgcenteno
 */
public class FixedLengthString {
    private final static int LENGTH = 5;
    private String value;

    /**
     * Construye una FixedLengthString en base a la palabra pasada como parámetro
     * @param value Palabra contenida en el objeto
     */
    public FixedLengthString(String value) {   
        java.util.Objects.requireNonNull(value);
        if(value.length() != LENGTH){
            throw new IllegalArgumentException("Only " + LENGTH + " letter words are allowed");
        }
        else if(!value.matches("[A-Za-z]{5}")){
            throw new IllegalArgumentException("Only A-Z a-z are allowed");
        }
        this.value = value;
    }

    /**
     * Devuelve la palabra 
     * @return String con la palabra
     */
    public String getValue() {
        return value;
    }        
}
