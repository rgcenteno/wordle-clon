/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wordle.wordleclass;

/**
 * Enumerado para conocer si una letra existe en la palabra, no existe o está en posición correcta.
 * @author rgcenteno
 */
public enum Resultado {
    /**
     * La letra no existe en la palabra. Valor 1.
     */
    MAL, 
    /**
     * La letra existe en la palabra pero no está en su posición óptima. Valor 2.
     */
    EXISTE, 
    /**
     * La letra existe y está en la posición en la que debe de estar. Valor 3.
     */
    OK;
    
    /**
     * Devuelve un resultado en base a su posición en base 1
     * @param eleccion valor entre 1 y longitud
     * @return Resultado en la posición dada
     */
    public static Resultado of(int eleccion) {
        java.util.Objects.checkIndex(eleccion - 1, Resultado.values().length);
        return Resultado.values()[eleccion - 1];
    }
}
