/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wordle.wordleclass;

/**
 * Interface a implementar para permitir que se trabaje con la interfaz de Wordle
 * @author rgcenteno
 */
public interface IMotorIdioma {  
    /**
     * Comprueba si la palabra recibida como parámetro existe en el diccionario
     * @param palabra la palabra que debemos comprobar
     * @return 
     */
    public boolean checkPalabra(String palabra);
    /**
     * Debe devolver una palabra aleatoria del diccionario
     * @return 
     */
    public FixedLengthString obtenerPalabraAleatoria();
    
}
